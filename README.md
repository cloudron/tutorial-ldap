## LDAP Cloudron App Packaging tutorial

Read the tutorial at https://cloudron.io/tutorials/packaging.html

## Testing

```
  # Test with various credentials here. Your cloudon admin username and password should succeed.
  curl -H 'X-Username: admin' -H 'X-Password: pass' https://my.cloudron.name/login
```

