'use strict';

var http = require('http'),
    ldap = require('ldapjs'),
    url = require('url');

var server = http.createServer(function (request, response) {
  var parsedUrl = url.parse(request.url, true /* parseQueryString */);

  if (parsedUrl.pathname === '/') {
      response.writeHead(200, { 'Content-Type': 'text/plain' });
      response.write('Authenticate with Cloudron credentials using the following command:\n\n');
      response.write('NOTE: Your password may need to be percent encoded: https://www.url-encode-decode.com/\n\n');
      response.end(`curl '${process.env.CLOUDRON_APP_ORIGIN}/auth?username=<username>&password=<password>'`);
  } else if (parsedUrl.pathname === '/auth') {
      var ldapClient = ldap.createClient({ url: process.env.CLOUDRON_LDAP_URL });
      var username = parsedUrl.query.username || '';
      var password = parsedUrl.query.password || '';
      var ldapDn = 'cn=' + username + ',' + process.env.CLOUDRON_LDAP_USERS_BASE_DN;

      ldapClient.bind(ldapDn, password, function (error) {
        if (error) {
          response.writeHead(401, { 'Content-Type': 'text/plain' });
          response.end('Failed to authenticate: ' + error + '\n');
        } else {
          response.writeHead(200, { 'Content-Type': 'text/plain' });
          response.end('Successfully authenticated\n');
        }
        ldapClient.unbind();
      });
  }
});

server.listen(8000);

console.log('Server running at port 8000');

